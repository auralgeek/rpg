extern crate ncurses;

use ncurses::*;
use std::collections::HashMap;
use std::{thread, time};
use rand::distributions::{Alphanumeric, Standard};
use rand::{Rng, thread_rng};
use rand_distr::{Distribution, Normal, Uniform};


#[derive(Debug)]
enum ActorRace {
    Human,
    BeastMan,
    HighElf,
    DarkElf,
    Dwarf,
    Gnome,
}

impl Distribution<ActorRace> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> ActorRace {
        match rng.gen_range(0, 6) {
            0 => ActorRace::Human,
            1 => ActorRace::BeastMan,
            2 => ActorRace::HighElf,
            3 => ActorRace::DarkElf,
            4 => ActorRace::Dwarf,
            _ => ActorRace::Gnome,
        }
    }
}

#[derive(Debug)]
struct ActorStats {
    strength: u32,
    endurance: u32,
    agility: u32,
    speed: u32,
    intelligence: u32,
    willpower: u32,
    personality: u32,
    luck: u32,
}

trait Entity {
    fn draw(&self, map: &mut HashMap<(i32, i32), Tile>);
}

#[derive(Debug)]
struct Actor {
    name: String,
    race: ActorRace,
    hp: i32,
    mp: i32,
    sp: i32,
    stats: ActorStats,
    melee_weapon: Weapon,
    position: (i32, i32),
}

impl Actor {
    fn new(name: String, race: ActorRace, hp: i32, mp: i32, sp: i32, stats: ActorStats, melee_weapon: Weapon, position: (i32, i32)) -> Actor {
        Actor { name, race, hp, mp, sp, stats, melee_weapon, position }
    }

    fn calc_physical_damage(&self) -> i32 {
        ((self.stats.strength as f64 / 10.0) * self.melee_weapon.base_attack as f64) as i32
    }

    fn take_damage(&mut self, base_damage: i32) {
        self.hp -= base_damage;
        println!("{} took {} damage!", self.name, base_damage);
        if self.hp <= 0 {
            println!("{} died!", self.name);
        }
    }

    fn move_to(&mut self, new_position: (i32, i32)) {
        self.position = new_position;
    }

    fn move_relative(&mut self, relative_movement: (i32, i32)) {
        self.position.0 += relative_movement.0;
        self.position.1 += relative_movement.1;
    }
}

impl Entity for Actor {
    fn draw(&self, map: &mut HashMap<(i32, i32), Tile>) {
        let mut icon = String::from("H");
        match self.race {
            ActorRace::Human => icon = String::from("H"),
            ActorRace::BeastMan => icon = String::from("B"),
            ActorRace::HighElf => icon = String::from("E"),
            ActorRace::DarkElf => icon = String::from("R"),
            ActorRace::Dwarf => icon = String::from("D"),
            ActorRace::Gnome => icon = String::from("G"),
            _ => {},
        }
        map.insert(self.position, Tile { name: self.name.clone(), icon: icon });
    }
}

fn get_random_actor_stats() -> ActorStats {
    let mut rng = thread_rng();
    let dist = Normal::new(100.0, 10.0).unwrap();
    let strength = dist.sample(&mut rng) as u32;
    let endurance = dist.sample(&mut rng) as u32;
    let agility = dist.sample(&mut rng) as u32;
    let speed = dist.sample(&mut rng) as u32;
    let intelligence = dist.sample(&mut rng) as u32;
    let willpower = dist.sample(&mut rng) as u32;
    let personality = dist.sample(&mut rng) as u32;
    let luck = dist.sample(&mut rng) as u32;

    ActorStats { strength, endurance, agility, speed, intelligence, willpower, personality, luck }
}

fn get_random_npc() -> Actor {
    let mut rng = thread_rng();
    let dist = Normal::new(100.0, 10.0).unwrap();
    let name: String = Alphanumeric.sample_iter(rng).take(7).collect();
    let race: ActorRace = rand::random();
    let hp = dist.sample(&mut rng) as i32;
    let mp = dist.sample(&mut rng) as i32;
    let sp = dist.sample(&mut rng) as i32;
    let stats = get_random_actor_stats();
    let weapon = Weapon::new(String::from("Unarmed"), 5, 100);
    let unif = Uniform::from(0..32);
    let position = (unif.sample(&mut rng), unif.sample(&mut rng));

    Actor::new(name, race, hp, mp, sp, stats, weapon, position)
}

struct GameMap {
    max_x: i32,
    max_y: i32,
    tiles: HashMap<(i32, i32), Tile>,
    actors: HashMap<(i32, i32), Tile>,
}

#[derive(Debug)]
struct Weapon {
    name: String,
    base_attack: u32,
    durability: u32,
}

impl Weapon {
    fn new(name: String, base_attack: u32, durability: u32) -> Weapon {
        Weapon {
            name,
            base_attack,
            durability,
        }
    }
}

impl ActorStats {
    fn new(
        strength: u32,
        endurance: u32,
        agility: u32,
        speed: u32,
        intelligence: u32,
        willpower: u32,
        personality: u32,
        luck: u32,
    ) -> ActorStats {
        ActorStats {
            strength,
            endurance,
            agility,
            speed,
            intelligence,
            willpower,
            personality,
            luck,
        }
    }
}

fn combat(actor1: &mut Actor, actor2: &mut Actor) {
    let first: &mut Actor;
    let second: &mut Actor;
    if actor1.stats.speed < actor2.stats.speed {
        first = actor2;
        second = actor1;
    } else {
        first = actor1;
        second = actor2;
    }

    loop {
        second.take_damage(first.calc_physical_damage());
        if second.hp <= 0 {
            return
        }

        first.take_damage(second.calc_physical_damage());
        if first.hp <= 0 {
            return
        }
    }
}

struct Tile {
    name: String,
    icon: String,
}

fn print_view(map: &HashMap<(i32, i32), Tile>) {
    for (pos, tile) in map.iter() {
        mvprintw(pos.0 as i32, pos.1 as i32, &tile.icon);
    }
}

fn game_loop(player: &mut Actor, map: &mut GameMap) {
    initscr();
    noecho();
    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);
    keypad(stdscr(), true);
    print_view(&map.tiles);
    print_view(&map.actors);
    refresh();
    let mut ch = getch();
    loop {
        match ch {
            KEY_LEFT => {
                if player.position.1 > 0 {
                    map.actors.remove(&player.position);
                    player.move_relative((0, -1));
                    player.draw(&mut map.actors);
                }
            },
            KEY_RIGHT => {
                if player.position.1 < map.max_x - 1 {
                    map.actors.remove(&player.position);
                    player.move_relative((0, 1));
                    player.draw(&mut map.actors);
                }
            },
            KEY_UP => {
                if player.position.0 > 0 {
                    map.actors.remove(&player.position);
                    player.move_relative((-1, 0));
                    player.draw(&mut map.actors);
                }
            },
            KEY_DOWN => {
                if player.position.0 < map.max_y - 1 {
                    map.actors.remove(&player.position);
                    player.move_relative((1, 0));
                    player.draw(&mut map.actors);
                }
            },
            KEY_HOME => {
                break;
            },
            _ => {},
        }
        print_view(&map.tiles);
        print_view(&map.actors);
        refresh();
        ch = getch();
    }
    clear();
    mv(0, 0);
    endwin();
}

fn create_blank_map() -> GameMap {
    let mut actor_map: HashMap<(i32, i32), Tile> = HashMap::new();
    let mut tile_map: HashMap<(i32, i32), Tile> = HashMap::new();
    let max_x = 32;
    let max_y = 32;
    for i in 0..max_y {
        for j in 0..max_x {
            tile_map.insert((i, j), Tile { name: String::from("Grass"), icon: String::from(".") });
        }
    }

    GameMap { tiles: tile_map, actors: actor_map, max_x, max_y }
}


fn main() {

    // Create map
    let mut map = create_blank_map();

    // Create player character
    let joestats = ActorStats::new(35, 35, 35, 35, 35, 35, 35, 35);
    let iron_sword = Weapon::new(String::from("Iron Sword"), 10, 100);
    let mut joe = Actor::new(String::from("Joe"), ActorRace::Human, 100, 100, 100, joestats, iron_sword, (10, 11));
    joe.draw(&mut map.actors);

    // Create some random NPCs
    let mut npcs = vec![];
    for _ in 0..10 {
        npcs.push(get_random_npc());
    }

    for npc in &mut npcs {
        npc.draw(&mut map.actors);
    }

    // Start the game loop
    game_loop(&mut joe, &mut map);
}
