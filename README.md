RPG
===

Making a thing like an RPG I guess.

Dependencies
------------

Using rust to build this, so you'll need that.  As of right now the only
dependency is ncurses, to build I ran on Ubuntu:

``` sh
$ apt install libncurses5-dev
```

Build with:

``` sh
$ cargo build
```
